# 💻 Backend challenge

Seja bem-vindo ao desafio Backend da Gobrax!

O objetivo deste desafio é avaliar suas habilidades de programação no desenvolvimento de soluções backend, independentemente do seu nível (júnior, pleno ou sênior). 
Assim que concluir o desafio, responda o email que recebeu com o link do seu repositório. 

Forneceremos feedback e instruções para os próximos passos!


Bom desafio!

> ⚠️ **Certifique-se de que seu repositório esteja público para avaliação.**

# 🧠 Contexto

Nossa empresa foca na eficiência do uso de combustível por motoristas. Para isso, queremos que você desenvolva uma aplicação backend que permita o cadastro e vinculação de motoristas e veículos.

## Funcionalidades a serem implementadas:

- [ ] CRUD de motoristas (Criação, Listagem, Atualização e Remoção)
- [ ] CRUD de veículos (Criação, Listagem, Atualização e Remoção)
- [ ] Vinculação de motorista a um veículo

## 📋 Instruções

É hora de começar a codificar!

- Utilize Golang para o desenvolvimento.
- Utilize um banco de dados de sua escolha (pode ser SQLite, PostgreSQL, etc.).
- Utilize Docker para empacotar suas dependências e aplicação.
- Inclua instruções de instalação do projeto no README.

Sinta-se à vontade para adicionar quaisquer observações relevantes.

**Observação Importante:**
> Um código bem feito é um código testado. Não existe teste feito, feio é não ter teste.


Vamos trabalhar juntos para otimizar o consumo de combustível e economizar 1 bilhão de litros juntos?! 💚